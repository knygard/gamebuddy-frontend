# GameBuddy

GameBuddy is a matchmaking service for offline activities. Matchmaking can be found in video games, where people play against each other online. Also, in competitive and administered sports the opponents are automatically selected. However, there is no possibility for casual players to find opponents offline. GameBuddy aims to make it easy to find a suitable opponent or a team for any sport or game. Whether you play tennis, ice-hockey, surf-boarding, climbing or chess, you could use GameBuddy to find a friend or establish a team.

## Technology

Frontend: __AngularJS__

Other: __Grunt__, __Bower__