'use strict';

angular.module('gamebuddy')

.directive('buttonGroup', function() {
  return {
    restrict: 'EA',
    transclude: true,
    replace: true,
    scope: {
      choices: '=',
      ngModel: '=',
      choiceKey: '@',
      disabled: '='
    },
    template: '<div ng-transclude class="button-group"></div>',
    controller: function($scope) {
      var allChoices = $scope.allChoices = [];
      var savedActiveChoice = undefined; // save to memory in case of disabled state

      this.addChoice = function(choice) {
        if (allChoices.length === 0) {// activate first item
          choice.active = true;
          this.select(choice);
        };

        // special case: remove active from first if
        // another (this) item is already active
        if (choice.active)
          _.first(allChoices, function(c) { c.active = false; });

        allChoices.push(choice);
      };

      this.select = function(choice) {
        if ($scope.choiceKey)
          $scope.ngModel = choice[$scope.choiceKey];
        else $scope.ngModel = choice;

        angular.forEach(allChoices, function(choice) {
          choice.active = false;
        });
        choice.active = true;
      };

      // handle disabled state
      $scope.$watch('disabled', function(newDisabled, oldDisabled) {
        if (newDisabled && newDisabled.length > 0 && !savedActiveChoice) {
          savedActiveChoice = _.find(allChoices, {'active': true});
          if (savedActiveChoice) savedActiveChoice.active = false;
        } else if (!newDisabled) {
          if (!_.find(allChoices, {'active': true}) && savedActiveChoice)
            savedActiveChoice.active = true;
          savedActiveChoice = undefined;
        }
      });
    }
  }
}).directive('gbBoxChoice', function() {
  return {
    require: '^buttonGroup',
    restrict: 'EA',
    templateUrl: '../templates/directives/gb-box-choice.html',
    transclude: true,
    scope: {
      choice: '=',
      icon: '='
    },
    replace: true,
    link: function(scope, element, attrs, parentCtrl) {
      parentCtrl.addChoice(scope.choice);

      element.on('click', function() {
        parentCtrl.select(scope.choice);
        scope.$apply();
      });
    }
  }
}).directive('gbButton', function() {
  return {
    require: '^buttonGroup',
    restrict: 'EA',
    template: '<div class="btn" ng-class="{active: choice.active}" ng-transclude></div>',
    replace: true,
    transclude: true,
    scope: {
      choice: '='
    },
    link: function(scope, element, attrs, parentCtrl) {
      parentCtrl.addChoice(scope.choice);

      element.on('click', function() {
        parentCtrl.select(scope.choice);
        scope.$apply();
      });
    }
  }
})

.directive('backButton', function($window) {
  return {
    restrict: 'EA',
    template: '<div class="back-button" ng-transclude></div>',
    transclude: true,
    link: function(scope, element, attrs) {
      element.on('click', function() {
        $window.history.back();
      });
    }
  }
})

.directive('loader', function() {
  return {
    restrict: 'EA',
    template: '<span ng-if="loadIf" class="loader"><i class="fa fa-spin fa-spinner"></i> loading</span>',
    transclude: true,
    scope: { loadIf: '=' }
  }
})

.directive('notifications', function() {
  return {
    restrict: 'AE',
    replace: false,
    templateUrl: '../templates/directives/notifications.html',
    scope: {
      identifier: '@' // dict object to specify which notifications to show
    },
    controller: function($scope, NotificationService) {
      $scope.notifications = [];

      $scope.$watch(function () {
        if ($scope.identifier) return NotificationService.get($scope.identifier);
        else return NotificationService.get();
      }, function (notifications, oldNotifications) {
        if (notifications && notifications !== oldNotifications)
          $scope.notifications = notifications;
      }, true);

      $scope.removeNotification = function(notification) {
        NotificationService.remove(notification);
      }
    }
  }
})

.directive('settingGroup', function() {
  return {
    restrict: 'AE',
    replace: true,
    template: '<div ng-transclude></div>',
    transclude: true,
    scope: false,
    controller: function($scope) {
      var settings = [];

      this.addSetting = function(setting) {
        settings.push(setting);
      };

      this.editSetting = function(thisSetting) {
        settings.forEach(function(setting) {
          if (thisSetting !== setting)
            setting.edit = false;
          else setting.edit = true;
        });
      };
    }
  };
})

.directive('setting', function() {
  return {
    restrict: 'AE',
    replace: true,
    require: '^settingGroup',
    transclude: false,
    templateUrl: '../templates/directives/setting.html',
    scope: {
      'title': '@',
      'content': '=',
      'editable': '@',
      'type': '@'
    },
    link: function(scope, element, attrs, settingGroupCtrl) {
      settingGroupCtrl.addSetting(scope);

      element.bind("keydown keypress", function (event) {
        if (event.which === 13)
          scope.$apply(function() {
            scope.save();
          });
      });

      scope.editMode = function() {
        settingGroupCtrl.editSetting(scope);
        scope.tempObject = angular.copy(scope.content);

        // set focus to input
        setTimeout(function() {
          angular.element(element[0].querySelector('input'))[0].focus();
        }, 0);
      };

      scope.cancel = function() {
        scope.edit = false;
      };

      scope.save = function() {
        scope.edit = false;
        scope.content = scope.tempObject;
      };
    }
  };
})

.directive('gbMap', function() {
  return {
    restrict: 'AE',
    replace: true,
    transclude: false,
    template: '<div id="map-canvas" style="width: 100%; height: 100%;"></div>',
    scope: {
      resize: '=',
      markers: '=',
      openMarker: '=',
      geoLocation: '='
    },
    controller: function($scope, $timeout) {
      $scope.mapOptions = {
        zoom: 13,
        center: new google.maps.LatLng(60.184, 24.830) // initial coordinates
      };

      $scope.$watch('geoLocation', function(newGeoLoc, oldGeoLoc) {
        if (newGeoLoc !== undefined && newGeoLoc)
          map.panTo(new google.maps.LatLng($scope.geoLocation.latitude, $scope.geoLocation.longitude));
      }, true);

      var markers = [];
      var map = new google.maps.Map(document.getElementById('map-canvas'),
        $scope.mapOptions);

      google.maps.event.addListenerOnce(map, 'idle', function() {
        google.maps.event.trigger(map, 'resize');
      });

      function showMarkers() {
        markers.forEach(function(marker) {
          marker.setMap(map);
        });
      };

      function clearMarkers() {
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers.length = 0;
      };

      // generate markers
      $scope.$watch('markers', function(newMarkers, oldMarkers) {
        if (newMarkers !== oldMarkers) {
          clearMarkers();

          newMarkers.forEach(function(marker) {
            var gmarker = new google.maps.Marker({
              position: new google.maps.LatLng(marker.coordinates.latitude, marker.coordinates.longitude),
              title: marker.description
            });
            gmarker.addListener('click', function() {
              $scope.openMarker(marker.id);
              $scope.$apply();
            });
            markers.push(gmarker);
          });

          showMarkers();
        };
      });

      $scope.resize = function() {
        $timeout(function() { // wait for dom-rendering
          google.maps.event.trigger(map, 'resize');
       }, 100)
      };
    }
  }
});
