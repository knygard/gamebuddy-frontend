'use strict';

angular.module('gamebuddy', [
  'ngResource', 'ngAnimate', 'ngSanitize', 'ngCookies',
  'ui.router', 'ui.bootstrap'
])

.constant('APIHost', '@@apihost') // @@apihost auto-replaced by Grunt based on AppConfig.json
.constant("AuthToken", {
  'cookieName': 'authtoken',
  'headerName': 'Authorization'
})

.config(function($locationProvider) {

  $locationProvider
    .html5Mode(true)
    .hashPrefix('!');
})

.run(function($cookieStore, AuthToken, AuthAPI) {
  /* Get user if already logged in */
  if ($cookieStore.get(AuthToken.cookieName)) {
    AuthAPI.setToken($cookieStore.get(AuthToken.cookieName));
  }
});
