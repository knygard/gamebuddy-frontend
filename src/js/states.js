'use strict';

angular.module('gamebuddy')

.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider
    .otherwise('/browse');

  var tdir = '../templates/';

  $stateProvider
    .state('master', {
      abstract: true,
      templateUrl: tdir + 'master.html',
      controller: 'MasterController',
      resolve: {
        isAuthenticated: function(AuthAPI, $state) {
          return AuthAPI.isAuthenticated();
        }
      }
    })

    .state('master.auth', {
      abstract: true,
      templateUrl: tdir + 'auth.html',
      controller: 'LoginController'
    })
    .state('master.auth.login', {
      url: '/login',
      templateUrl: tdir + 'auth.login.html',
      resolve: {
        isAuthenticated: function(AuthAPI, $state) {
          return AuthAPI.isAuthenticated().then(function(isAuthenticated) {
            if (isAuthenticated) $state.go('master.content.browse');
          });
        }
      }
    })
    .state('master.auth.register', {
      url: '/register',
      templateUrl: tdir + 'auth.register.html',
      resolve: {
        isAuthenticated: function(AuthAPI, $state) {
          return AuthAPI.isAuthenticated().then(function(isAuthenticated) {
            if (isAuthenticated) $state.go('master.content.browse');
          });
        }
      }
    })

    .state('master.content', {
      abstract: true,
      templateUrl: tdir + 'content.html',
      resolve: {
        isAuthenticated: function(AuthAPI, $state) {
          return AuthAPI.isAuthenticated();
        }
      }
    })
    .state('master.content.browse', {
      url: '/browse',
      templateUrl: tdir + 'content.browse.html',
      controller: 'BrowseController'
    })
    .state('master.content.search', {
      url: '/search',
      templateUrl: tdir + 'content.search.html'
    })
    .state('master.content.add', {
      url: '/add',
      templateUrl: tdir + 'content.add.html',
      controller: 'NewAnnouncementController'
    })

    .state('master.profile', {
      abstract: true,
      templateUrl: tdir + 'profile.html',
      url: '/profile',
      resolve: {
        isAuthenticated: function(AuthAPI, $state) {
          return AuthAPI.isAuthenticated().then(function(isAuthenticated) {
            if (!isAuthenticated) $state.go('master.auth.login');
          });
        }
      }
    })
    .state('master.profile.settings', {
      url: '/settings',
      templateUrl: tdir + 'profile.settings.html',
      controller: 'SettingsController'
    })
    .state('master.profile.messages', {
      url: '/messages',
      templateUrl: tdir + 'profile.messages.html',
      controller: 'MessagesController'
    }).state('master.profile.messages_new', {
        url: '/messages/user/:userId',
        templateUrl: tdir + 'profile.messages.html',
        controller: 'MessagesController'
      })
    .state('master.profile.announcements', {
      url: '/myannouncements',
      templateUrl: tdir + 'profile.announcements.html',
      controller: 'MyAnnouncementsController'
    })

    .state('master.announcement', {
      url: '/announcements/:id',
      templateUrl: tdir + 'announcement.html',
      controller: 'SingleAnnouncementController',
      resolve: {
        announcement: function(AnnouncementAPI, $stateParams, $q) {
          return AnnouncementAPI.resource.get({id: $stateParams.id});

          var deferred = $q.defer();
          AnnouncementAPI.resource.get({id: $stateParams.id}, function(result) {
            deferred.resolve(result);
          }, function(result) {
            deferred.reject(result);
          });

          return deferred.promise;
        }
      }
    })
});
