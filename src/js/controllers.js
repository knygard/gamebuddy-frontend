'use strict';

angular.module('gamebuddy')

.controller('LoginController', function($scope, AuthAPI, $state, NotificationService) {
  $scope.login = function(user) {
    AuthAPI.login(user).success(function(response) {
      NotificationService.clear();
      $state.go('master.content.browse');
    }).error(function(response) {
      NotificationService.add("error", "Wrong username and/or password.");
    });
  };

  $scope.newUser = {};
  $scope.register = function(user) {
    AuthAPI.register($scope.newUser).success(function(response) {
      NotificationService.add("success", "Registration successful! Please log in.");
      $state.go('master.auth.login');
    }).error(function(response) {
      NotificationService.add("error", "Required fields are marked with '*' – please fill them in.");
    });
  };
})

.controller('MasterController', function($scope, AuthAPI, $state, NotificationService,
                                         isAuthenticated, ActivityTypesAPI, ActivityAPI) {

  if (isAuthenticated)
    $scope.user = AuthAPI.user();

  // watch for user changes
  $scope.$watch(AuthAPI.user, function(user) {
    $scope.user = AuthAPI.user();
  }, true);

  $scope.logout = function() {
    AuthAPI.logout().success(function(response) {
      NotificationService.clear();
      $state.go('master.content.browse', {}, {reload: true});
    });
  };

  $scope.new_announcement = {}; // initialize here to keep in memory

  $scope.skillChoices = [
    {name: "novice", value: 1},
    {name: "intermediate", value: 4},
    {name: "advanced", value: 7},
    {name: "pro", value: 10}
  ];
  $scope.genderTypes = [
    {name: 'both', value: ''},
    {name: 'male', value: 'm'},
    {name: 'female', value: 'f'}
  ];

  $scope.isActive = function(state) { // active class for nav
    return $state.includes(state);
  };

  $scope.$on('$stateChangeSuccess', function() {
    $scope.isCollapsed = true;
  });
})

.controller('ModalController', function ($scope, $modalInstance, announcement, AnnouncementAPI,
                                         NotificationService, $state) {

  $scope.announcement = announcement;

  $scope.submit = function () {
    AnnouncementAPI.save({
        'activity_type': $scope.announcement.activity_type.id,
        'activity': $scope.announcement.other ?
          $scope.announcement.other : $scope.announcement.activity.name,
        'location': $scope.announcement.location,
        gender_preference: $scope.announcement.gender_preference.value,
        'skill': $scope.announcement.skill.value,
        'description': $scope.announcement.description
      })
      .success(function () {
        NotificationService.add("success", "New announcement added succesfully!");
        $state.go("master.content.browse");
      })
      .error(function () {
        NotificationService.add("error", "There was an error in submitting your announcement."
                                         + " Please try again.")
      });

    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})

.controller('NewAnnouncementController', function($scope, $state, $modal, ActivityTypesAPI,
                                                  ActivityAPI, PlaceAPI, NotificationService) {

  $scope.activityTypes = ActivityTypesAPI.query();
  $scope.activities = ActivityAPI.query();
  $scope.locationChoices = PlaceAPI.query();

  $scope.validate = function () {
    var requiredFields = [
      {key: 'location', description: 'location'},
      {key: 'activity', description: 'activity'},
      {key: 'activity_type', description: 'activity type'},
      {key: 'gender_preference', description: 'gender preference'},
      {key: 'skill', description: 'skill level'}
    ];

    return !_.some(requiredFields,
      function (requiredField) {
        if (!$scope.new_announcement.hasOwnProperty(requiredField.key)
            || !$scope.new_announcement[requiredField.key]) {
          NotificationService.addObj({
            'type': "error",
            'content': requiredField.description + ' needs to be set',
            'identifier': 'submitForm'
          });
          return true;
        }
      });
  };

  $scope.confirm = function (size) {
    if (!$scope.validate()) {
      if (!$scope.new_announcement.description)
        NotificationService.addObj({
          type: "warning",
          content: "Are you sure you do not want to set description?",
          identifier: "submitForm"
        });
      return
    }

    var modalInstance = $modal.open({
      templateUrl: '../templates/modal.add-confirm.html',
      controller: 'ModalController',
      size: size,
      resolve: {
        announcement: function () {
          return $scope.new_announcement;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      // modal dismiss
    });
  };

  $scope.$watch('new_announcement.activity_type', function(newType, oldType) {
    if (newType && newType !== oldType)
      $scope.activities = ActivityAPI.query({activity_type: newType.id});
  });

  $scope.$watch('new_announcement.activity', function(newActivity, oldActivity) {
    if (newActivity)
      $scope.new_announcement.other = '';
  });
})

.controller('MessagesController', function($scope, AuthAPI, MessagesAPI, $stateParams,
                                           NotificationService, $state) {

  $scope.loading = true;

  $scope.activeDiscussion = {};
  $scope.newMessage = {'sender': $scope.user.id, 'content': ''};

  MessagesAPI.discussions.query({id: $scope.user.id}, function (result) {
    $scope.loading = false;
    $scope.discussions = result;

    if ($stateParams.hasOwnProperty('userId') && $stateParams.userId !== $scope.user.id) {
      if (!$scope.setActiveDiscussion())
        MessagesAPI.discussions.save( // create new discussion
              {user1: $scope.user.id, user2: $stateParams.userId}, function (result) {
          $scope.loading = true;
          MessagesAPI.discussions.query({id: $scope.user.id}, function (result) {
            $scope.loading = false;
            $scope.discussions = result;
            $scope.setActiveDiscussion();
          });
        });
    }
  });

  $scope.setActiveDiscussion = function() {
    $scope.discussions.forEach(function (discussion) {
      if ($scope.notMe(discussion).id == $stateParams.userId) {
        $scope.activeDiscussion = discussion;
        $scope.activeDiscussion.messages = MessagesAPI.getDiscussion($scope.user.id, $scope.notMe(discussion).id);
        return true;
      }
    });
    return false;
  };

  $scope.notMe = function(discussion) {
    if ($scope.user.id === discussion.user1.id)
      return discussion.user2;
    return discussion.user1;
  };

  $scope.open = function(discussion) {
    $state.go('master.profile.messages_new', {userId: $scope.notMe(discussion).id});
  };

  $scope.send = function () {

    $scope.activeDiscussion.messages.push({
      "sender": {"username": $scope.user.username},
      "content": $scope.newMessage.content,
      "created": "just now",
      "read": true
    });

    MessagesAPI.send($scope.user.id,
                     $scope.activeDiscussion.user2.id,
                     $scope.newMessage.content,
                     $scope.activeDiscussion.id);
  };

  $scope.getOther = function(discussion) {
    if (discussion.user1.id === $scope.user.id)
      return $scope.notMe(discussion);
    else if ($scope.notMe(discussion).id === $scope.user.id)
      return discussion.user1;
  };

  $scope.remove = function(announcement) {
    NotificationService.add("warning", "Sorry, removing discussions is not yet supported!");
  };
})

.controller('SingleAnnouncementController', function ($scope, AnnouncementAPI, NotificationService, $state,
                                                      announcement, MessagesAPI) {

  if (!$scope.announcement && announcement)
    $scope.announcement = announcement;

  $scope.message = function() {
    $state.go('master.profile.messages_new', {userId: $scope.announcement.creator.id});
  }

  $scope.addQuestion = function(question) {
    var question = {'content': question.content};
    question.user = AuthAPI.user();
    question.created = new Date();
    if (!$scope.announcement.questions)
      $scope.announcement.questions = []; //temporary hack
    $scope.announcement.questions.push(question);
  }
})

.controller('SingleAnnouncementModalController', function ($scope, $modalInstance, announcement, $state,
                                                           MessagesAPI, user) {

  if (!$scope.announcement && announcement)
    $scope.announcement = announcement;

  if (!$scope.user && user)
    $scope.user = user;

  $scope.submit = function () {
    $modalInstance.close();
  }

  $scope.open = function () {
    $state.go('master.announcement', {id: $scope.announcement.id});
    $modalInstance.close();
  }

  $scope.message = function() {
    $state.go('master.profile.messages_new', {userId: $scope.announcement.creator.id});
    $modalInstance.close();
  }

  $scope.close = function () {
    $modalInstance.dismiss('cancel');
  }
})

.controller('BrowseController', function($scope, $state, AnnouncementAPI, GeoLocationService,
                                         ActivityTypesAPI, ActivityAPI, MessagesAPI, $location, $modal) {

  $scope.activityTypes = ActivityTypesAPI.query();
  $scope.activities = ActivityAPI.query();

  $scope.objects_per_page = 12;
  $scope.currentPage = 1;

  $scope.getAnnouncements = function() {
    AnnouncementAPI.resource.get(
      {
        'objects_per_page': $scope.objects_per_page,
        'page': $scope.currentPage,
        'activity_type': $scope.filter.activity_type,
        'gender_preference': $scope.filter.gender_preference,
        'skill': $scope.filter.skill_level
      },
      function(result) {
        $scope.announcements = result.data;
        $scope.announcementsCount = result.meta.objectcount;
      }
    )
  }

  GeoLocationService.getLocation().then(function(Geoposition) {
    $scope.geoLocation = {
      "latitude": Geoposition.coords.latitude,
      "longitude": Geoposition.coords.longitude,
    }
  });

  // filtering null-fix
  $scope.$watch('filter', function(filter, oldFilter) {
    if (filter !== oldFilter)
      angular.forEach(filter, function(val, key) {
        if (val === null) filter[key] = '';
      });
  }, true);

  $scope.order = {};
  $scope.sort = function(property){
    if ($scope.order.property === property){
      $scope.order.reverse = !$scope.order.reverse;
      $scope.announcements.reverse();
    } else{
      $scope.order.property = property;
      $scope.order.reverse = false;
      $scope.announcements.sort(compare);
    }
  };

  function compare(a,b) {
    if (a[$scope.order.property] < b[$scope.order.property])
       return -1;
    if (a[$scope.order.property] > b[$scope.order.property])
      return 1;
    return 0;
  };

  $scope.selectAnnouncement = function(id) {
    $scope.activeAnnouncement = _.find($scope.announcements, function(a) {
      return a.id === id;
    });

    // open modal
    $scope.openAnnouncement($scope.activeAnnouncement);
  };

  // watch for filter changes
  $scope.$watch('filter', function(newFilter, oldFilter) {
    if (newFilter) {
      $scope.currentPage = 1;
      $scope.getAnnouncements();
    }
  }, true);

  // hide active announcement if it is not in filtered announcements
  $scope.$watch('filteredAnnouncements.length', function(newLength, oldLength) {
    if (newLength !== oldLength)
      if (!_.find($scope.filteredAnnouncements, $scope.activeAnnouncement))
        $scope.activeAnnouncement = undefined;
  });

  // create markers based on filtered announcements
  $scope.markers = [];
  $scope.$watch('filteredAnnouncements.length', function(newLength, oldLength) {
    if (newLength !== oldLength)
      $scope.markers = $scope.filteredAnnouncements.map(function(a) {
        return {
          "id": a.id,
          "coordinates": a.coordinates,
          "title": a.description
        };
      });
  });

  $scope.openAnnouncement = function (announcement) {
    var modalInstance = $modal.open({
      templateUrl: '../templates/modal.single-announcement.html',
      controller: 'SingleAnnouncementModalController',
      size: 'lg',
      resolve: {
        announcement: function () {
          return announcement;
        },
        user: function () {
          return $scope.user;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      // modal dismiss
    });
  };
})

.controller('PlacesController', function($scope, $http, PlaceAPI) {
  $scope.active = null;
  $scope.search = "";

  $scope.places = PlaceAPI.query();

  $scope.show = function(place) {
    $scope.active = place;
  };
})

.controller('MyAnnouncementsController', function($scope, AnnouncementAPI, NotificationService) {
  $scope.announcements = null;
  AnnouncementAPI.getMy($scope.user.id).then(function(res){
    $scope.announcements = res.data;
  });

  $scope.delete = function(announcement) {
    AnnouncementAPI.delete(announcement.id)
    .success(function() {
      var index = $scope.announcements.indexOf(announcement);
      $scope.announcements.splice(index, 1);
      NotificationService.add("success", "Announcement deleted succesfully.");
    }).error(function() {
      NotificationService.add("error", "There was a problem in deleting the announcement.");
    });
  };

})

.controller('SettingsController', function($scope, AuthAPI, NotificationService) {
  $scope.confirm = function() {
    AuthAPI.update($scope.user).success(function() {
      NotificationService.add("success", "Settings updated succesfully.");
    }).error(function() {
      NotificationService.add("error", "There was a problem in updating the settings.");
    });
  };

  $scope.$watch('user', function(newUser, oldUser) {
    if (newUser !== oldUser)
      AuthAPI.update($scope.user).success(function() {
        NotificationService.add("success", "Settings updated succesfully.");
      }).error(function() {
        NotificationService.add("error", "There was a problem in updating the settings.");
      });
  }, true);
});
