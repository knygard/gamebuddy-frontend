// 'use strict';

angular.module('gamebuddy')

.factory('AnnouncementAPI', function($resource, APIHost, $http) {

  var newAnnouncement; // saved announcement
  var api = APIHost + '/announcements';

  return {
    save: function(announcement) {
      return $http.post(api, announcement);
    },
    resource: $resource(api + '/:id'),
    get: function(params) {
      return $http.get(api);
    },
    load: function() {
      return newAnnouncement;
    },
    delete: function(id) {
      return $http.delete(api  + '/' + id.toString());
    },
    getMy: function(id) {
      return $http.get(api  + '/user/' + id.toString());
    }
  };
})

.factory('MessagesAPI', function($resource, APIHost, $q) {
  var messages = $resource(APIHost + '/messages/');
  var discussions = $resource(APIHost + '/discussions/:id', {id: '@id'});
  var discussion = $resource(APIHost + '/discussion/:id1/:id2', {id1: '@id1', id2: '@id2'});

  return {
    getDiscussions: function(userId) {
      return discussions.query({'id': userId});
    },
    getDiscussion: function(id1, id2, callBack) {
      return discussion.query({id1: id1, id2: id2});
    },
    openDiscussion: function(id1, id2) {
      var message = {
        "user1": id1,
        "user2": id2
      };
      var result = discussions.save(message);
      return result;
    },
    // markRead: function(user1id, user2id) {
    //   window.alert('markRead');
    // },
    exists: function(id1, id2) {
      var data = discussion.query({id1: id1, id2: id2});
      var result = (data.length > 0);
      // window.alert(data.length);
      return result;
    },
    send: function(user1id, user2id, content, discussion) {
      var message = {
        "sender": user1id,
        "receiver": user2id,
        "content": content,
        "discussion": discussion
      }
      messages.save(message);
    },
    discussion: discussion,
    discussions: discussions
  };
})

.factory('ActivityTypesAPI', function ($resource, APIHost) {
  return $resource(APIHost + '/activitytypes/:id', {id: '@id'});
})

.factory('ActivityAPI', function ($resource, APIHost) {
  return $resource(APIHost + '/activities');
})

.factory('AuthAPI', function($http, APIHost, $cookieStore, AuthToken, $q) {
  var user;

  return {
    load: function() {
      var self = this;
      return $http.get(APIHost + '/auth/load')
        .success(function(data) {
          user = data;
        }).error(function(response) {
          self.unSetToken(); // unset token because it is wrong
        });
    },
    isAuthenticated: function() {
      var deferred = $q.defer();
      if ($cookieStore.get(AuthToken.cookieName)) {
        this.load().success(function() {
          deferred.resolve(true);
        }).error(function() {
          deferred.reject(false);
        });
      } else {
        deferred.resolve(false);
      };
      return deferred.promise;
    },
    setToken: function(token) {
      /* set token into cookies */
      $cookieStore.put(AuthToken.cookieName, token);
      $http.defaults.headers.common[AuthToken.headerName] = 'Token ' + token;
    },
    unSetToken: function() {
      $cookieStore.remove(AuthToken.cookieName);
      $http.defaults.headers.common[AuthToken.headerName] = undefined;
      user = undefined;
    },
    logout: function() {
      var self = this;
      return $http.get(APIHost + '/auth/logout')
        .success(function(data) {
          self.unSetToken();
        });
    },
    register: function(user) {
      return $http.post(APIHost + '/users/register', user);
    },
    user: function() {
      return user;
    },
    update: function(user) {
      return $http.put(APIHost + '/users/' + user.id.toString(), user);
    },
    login: function(user) {
      var self = this;
      return $http.post(APIHost + '/auth/login', user)
        .success(function(data) {
          /* set token into cookies */
          try { var authtoken = data.token; }
          catch(TypeError) { /* already logged */ };

          if (authtoken) self.setToken(authtoken);
        });
    }
  }
})

.factory('NotificationService', function($timeout) {
  var notifications = [];
  var register = [];

  return {
    add: function(type, content) {
      var notification = {"type": type, "content": content};
      this.addObj(notification);
    },
    addObj: function(notification) {
      var that = this;

      // check for required properties
      ['type', 'content'].forEach(function(type) {
        if (!notification.hasOwnProperty(type))
          console.error("notification needs property", type);
      });

      // add only if doesn't exist already
      if (!_.findWhere(notifications, notification)) {
        notifications.push(notification);
        $timeout(function() {
          notifications.splice(_.indexOf(notification), 1);
        }, 5000);
      }

      // set registered callbacks
      register.forEach(function(obj) {
        obj.callBack(that.get(obj.identifier));
      });
    },
    addList: function(listOfNotifications) {
      angular.forEach(listOfNotifications, function(notification) {
        this.add(notification.type, notification.content);
      });
    },
    get: function(identifier) {
      if (identifier)
        return _.where(notifications, {'identifier': identifier});

      // return notifications without key 'identifier'
      return notifications.filter(function(notification) {
        return (!notification.hasOwnProperty('identifier'));
      });
    },
    clear: function() {
      notifications.length = 0;
    },
    remove: function(notification) {
      notifications.splice(_.indexOf(notifications, notification), 1);
    },
    register: function(callBack, identifier) {
      var obj = {callBack: callBack, identifier: identifier};

      if (register.indexOf(obj) === -1)
        register.push(obj);
    }
  }
})

.factory('GeoLocationService', function($q, $rootScope) {
  var geoLocation;

  return {
    getLocation: function() {
      var deferred = $q.defer();
      if (geoLocation !== undefined) {
        deferred.resolve(geoLocation);
      } else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          geoLocation = position;
          deferred.resolve(position);
        });
      } else {
        $rootScope.$broadcast('error:geolocation');
        deferred.reject("Geolocation service is not available.");
      };

      return deferred.promise;
    }
  }
})

.factory('PlaceAPI', function($resource, APIHost) {
  return $resource(APIHost + '/places/:name', {name: '@name'});
});
